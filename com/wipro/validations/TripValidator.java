package com.wipro.validations;

import java.util.Random;

import com.wipro.bean.CabBean;
import com.wipro.userexceptions.NegativeKilometerException;

public class TripValidator {
public static String printBillAmount(CabBean cabbean)
{
	String str=cabbean.getBookingID().substring(2);//To check the last five digits are numbers, taking the substring from 2nd index till length-->12345
	String msg="";
	int arr[]=new int[2];
	try
	{
		//If Booking ID does not start with AD followed by 5-digit number , return invalid booking id
	if((!cabbean.getBookingID().startsWith("AD"))||(cabbean.getBookingID().length()!=7)||(!str.matches("[0-9]+")))
		msg="Invalid Booking ID";
		//If User ID is not a positive number ranges from 1001-1500 , return invalid user id
	else if(cabbean.getUserID()<0||cabbean.getUserID()<1001||cabbean.getUserID()>1500)
		msg="Invalid User ID";
		//If Cab type is not Tata Indica/Tata Indigo/BMU/Logan , return invalid cab type
	else if((!cabbean.getCabType().equalsIgnoreCase("Tata Indica"))&&(!cabbean.getCabType().equalsIgnoreCase("Tata Indigo"))&&(!cabbean.getCabType().equalsIgnoreCase("BMU"))&&(!cabbean.getCabType().equalsIgnoreCase("Logan")))
		msg="Invalid Cab Type";
		//If the Kms Used is not a positive number, throw user defined exception 
	else if(Integer.parseInt(cabbean.getKmsUsed())<0)
		throw new NegativeKilometerException();
	else
	{
	//If all the inputs are valid, generate the bill amount by calling amountGenerator method
	arr=amountGenerator(Integer.parseInt(cabbean.getKmsUsed()),cabbean.getCabType());
	//Printing the amount generated in the format ----> Total Amount : $$$ , Receipt ID : ###
	msg="Total amount : "+arr[1]+" , Receipt ID : "+arr[0];
	}
	}
	catch(NegativeKilometerException i)
	{
		msg=i.toString();
	}
	return msg;
	
}

private static int[] amountGenerator(int kmsUsed, String cabType) {
	// TODO Auto-generated method stub
	int billamt = 0;
	int arr[] = new int[2];
	//Generation of Random 5 digit Receipt Number using random method and assigning in the index 0 of integer array
	arr[0]=10000+new Random().nextInt(90000);//10000<=n<=99999
	//Generation of Bill Amount
	if(cabType.equalsIgnoreCase("Tata Indica"))
	{
		//If cab type is Tata Indica , charge is 12 Rs per Km
		billamt=12*kmsUsed;
	}
	if(cabType.equalsIgnoreCase("Tata Indigo"))
	{
		//If cab type is Tata Indigo , charge is 10 Rs per Km
		billamt=10*kmsUsed;
	}
	if(cabType.equalsIgnoreCase("BMU"))
	{
		//If cab type is BMU , charge is 45 Rs per Km
		billamt=45*kmsUsed;
	}
	if(cabType.equalsIgnoreCase("Logan"))
	{
		//If cab type is Logan , charge is 31 Rs per Km
		billamt=31*kmsUsed;
	}
	//Assigning the bill amount in index 1 of integer array
	arr[1]=billamt;
	return arr;
}
}
